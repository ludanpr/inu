# GNU Makefile
# Top-level makefile
#

# $(call source-to-object, source-file-list)
source-to-object = $(subst .c,.o,$(filter %.c,$1))

# $(subdirectory)
subdirectory = $(patsubst %/module.mk,%, $(word $(words $(MAKEFILE_LIST)),$(MAKEFILE_LIST)))

# $(call make-library, library-name, source-file-list)
define make-library
libraries += $1
sources   += $2

$1: $(call source-to-object,$2)
	$(AR) $(ARFLAGS) $$@ $$^               # defer evaluation with $$
endef

# Collect information from each module into these four variables.
# Initialize them here as simple variables.
modules     := INU/readtable/hash test #INU/readtable
programs    :=
libraries   :=
sources     :=

objects      = $(call source-to-object,$(sources))
dependencies = $(subst .o,.d,$(objects))

include_dirs := include INU/utils INU/sys
CPPFLAGS     += $(addprefix -I ,$(include_dirs))
vpath %.h $(include_dirs)

MV  := mv -f
RM  := rm -f
SED := sed

all:

include $(addsuffix /module.mk,$(modules))

.PHONY: all
all: $(programs)

.PHONY: libraries
libraries: $(libraries)

.PHONY: clean
clean:
	$(RM) $(objects) $(programs) $(libraries) $(dependencies)

ifneq ($(MAKECMDGOALS),clean)
    include $(dependencies)
endif

%.d: %.c
	$(CC) $(CFLAGS) $(CPPFLAGS) $(TARGET_ARCH) -M $< |              \
	$(SED) 's,\($(notdir $*)\.o\) *:,$(dir $@)\1 $@: ,' > $@.tmp
	$(MV) $@.tmp $@
