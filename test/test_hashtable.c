/**
 * @file test/test_hashtable.c
 */
#include <hashtable.h>
#include <assert.h>
#include <stddef.h>
#include <stdio.h>


struct testhe {
    struct htelem he;
    char data;
};


int cmpchar(struct htelem htep[static 1], void *key)
{
    struct testhe *tp = container_of(htep, struct testhe, he);
    return *((char *) key) == tp->data;
}

void test_htelem(void)
{
    inu_hashtable *ht = inu_hashtable_new(5, cmpchar);
    assert(ht);

    /* Should be empty */
    for (size_t i = 1; i <= 4; i++) {
        assert(!inu_hashtable_lookup(ht, i, (void *) &i));
    }

    struct testhe tes[4] = {
        [0] = (struct testhe){ .data = '#' },
        [1] = (struct testhe){ .data = '`' },
        [2] = (struct testhe){ .data = '\'' },
        [3] = (struct testhe){ .data = '@' },
    };

    for (size_t i = 0; i < 4; i++) {
        /* Asserting addition and lookup */
        inu_hashtable_add(ht, tes[i].data, &tes[i].he);
        struct htelem *htep = inu_hashtable_lookup(ht, tes[i].data, (void *) &tes[i].data);
        assert(htep);

        /* Asserting element integrity */
        struct testhe *tp = container_of(htep, struct testhe, he);
        assert(tp->data == tes[i].data);

        inu_hashtable_lookup_done(htep);
    }

    /* Asserting deletion */
    inu_hashtable_del(&tes[0].he);
    assert(!inu_hashtable_lookup(ht, tes[0].data, (void *) &tes[0].data));
    inu_hashtable_del(&tes[3].he);
    assert(!inu_hashtable_lookup(ht, tes[3].data, (void *) &tes[3].data));

    inu_hashtable_destroy(ht);
}

int main(void)
{
    test_htelem();

    printf("[%s]: *** All tests passed\n", __FILE__);

    return 0;
}
