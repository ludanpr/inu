local_pgm  := $(subdirectory)/test_hashtable # ...
local_src  := $(addprefix $(subdirectory)/,$(wildcard *.c))
local_objs := $(subst .c,.o,$(local_srcs))

programs   += $(local_pgm)
sources    += $(local_src)

$(local_pgm): CPPFLAGS += -I INU/readtable/hash
$(local_pgm): $(local_objs) $(libraries)
