/**
 * @file INU/utils/common.h
 */
#ifndef INU_COMMON_H_
#define INU_COMMON_H_ 1

#include <stddef.h>
#include <stdbool.h>

#ifndef FLEX_ARRAY
/* Check if the compiler is known to support flexible array members */
#  if defined(__STDC_VERSION__) && __STDC_VERSION__ >= 199901L && (!defined(__SUNPRO_C) || (__SUNPRO_C > 0x580))
#    define FLEX_ARRAY      /* empty */
#  elif defined(__GNUC__)
#    if __GNUC__ >= 3
#      define FLEX_ARRAY    /* empty */
#    else
#      define FLEX_ARRAY 0  /* older GNU extension */
#    endif
#  endif
#  ifndef FLEX_ARRAY
#    define FLEX_ARRAY 1    /* otherwise, default to safer but a bit wasteful traditional style. */
#  endif
#endif

#ifndef container_of
//#  if __STDC_VERSION__ >= 202XXX
//#    define MEMBER_TYPE(TYPE, MEMBER) typeof(((TYPE *) 0)->MEMBER)   // In case of C2X Standard typeof
//#  el
#  if defined(__clang__) || defined(__GNUC__)
#    define MEMBER_TYPE(TYPE, MEMBER) __typeof__(((TYPE *) 0)->MEMBER)
#  else          /* Defaults to a non-type-safe standard implementation of container_of */
#    define MEMBER_TYPE(TYPE, MEMBER) const void
#  endif
/**
 * @brief Returns a pointer to structure given address to one of
 * its members.
 */
#define container_of(PTR, TYPE, MEMBER) ((TYPE *)((char *)(MEMBER_TYPE(TYPE, MEMBER) *){ PTR } - offsetof(TYPE, MEMBER)))

#endif /* container_of */

/*---------------------------------------------------------------*/
#if __STDC_VERSION__ >= 201112L
#  if defined(__clang__)
#    if __has_feature(c_alignas)
#      define INU_HAS_ALIGNAS 1
#    endif
#  elif (defined(__GNUC__) && (__GNUC__ >= 5 || (__GNUC__ == 4 && __GNUC_MINOR__ >= 7)))
#    define INU_HAS_ALIGNAS 1
#  endif
#endif

#if !defined(CACHE_LINE_SIZE)
#  define CACHE_LINE_SIZE 64
#endif

#if !defined(INU_HAS_ALIGNAS)
typedef char ____cache_line_padding[CACHE_LINE_SIZE];
#endif

#endif
