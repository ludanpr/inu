/**
 * @file INU/sys/cdefs.h
 *
 *
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 1991, 1993
 *      The Regents of the University of California.  All rights reserved.
 *
 * This code is derived from software contributed to Berkeley by
 * Berkeley Software Design, Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
#ifndef SYS_CDEFS_H_
#define SYS_CDEFS_H_ 1

/*
 * Testing against CLANG-specific extensions.
 */
#ifndef __has_attribute
#define __has_attribute(x)       0
#endif
#ifndef __has_extension
#define __has_extension          __has_feature
#endif
#ifndef __has_feature
#define __has_feature(x)         0
#endif
#ifndef __has_include
#define __has_include(x)         0
#endif
#ifndef __has_builtin
#define __has_builtin(x)         0
#endif



/**
 * The following definitions are an extension of the behavior originally
 * implemented in <sys/_posix.h>, but with a different level of granularity.
 * POSIX.1 requires that the macros we test be defined before any standard
 * header file is included.
 *
 * Here's a quick run-down of the versions:
 *   defined(_POSIX_SOURCE)         1003.1-1988
 *    _POSIX_C_SOURCE == 1          1003.1-1990
 *    _POSIX_C_SOURCE == 2          1003.2-1992 C Language Binding Option
 *    _POSIX_C_SOURCE == 199309     1003.1b-1993
 *    _POSIX_C_SOURCE == 199506     1003.1c-1995, 1003.1i-1995, and the omnibus ISO/IEC 9945-1: 1996
 *    _POSIX_C_SOURCE == 200112     1003.1-2001
 *    _POSIX_C_SOURCE == 200809     1003.1-2008
 *
 * In addition, the X/Open Portability Guide, which is now the Single UNIX
 * Specification, defines a feature-test macro which indicates the version
 * of that specification, and which subsumes _POSIX_C_SOURCE.
 *
 * Our macros begin with two underscores to avoid namespace screwage.
 */

/*
 * Deal with IEEE Std. 1003.1-1990, in which _POSIX_C_SOURCE == 1
 */
#if defined(_POSIX_C_SOURCE) && _POSIX_C_SOURCE == 1
#  undef _POSIX_C_SOURCE           /* Probably illegal, but beyond caring now */
#  define _POSIX_C_SOURCE 199009
#endif

/*
 * Deal with IEEE Std. 1003.2-1992, in which _POSIX_C_SOURCE == 2.
 */
#if defined(_POSIX_C_SOURCE) && _POSIX_C_SOURCE == 2
#  undef _POSIX_C_SOURCE
#  define _POSIX_C_SOURCE 199209
#endif

/*
 * Deal with various X/Open Portability Guides and Single UNIX Spec.
 */
#ifdef _XOPEN_SOURCE
#  if _XOPEN_SOURCE - 0 >= 700
#    define __XSI_VISIBLE           700
#    undef _POSIX_C_SOURCE
#    define _POSIX_C_SOURCE      200809
#  elif _XOPEN_SOURCE - 0 >= 600
#    define __XSI_VISIBLE           600
#    undef _POSIX_C_SOURCE
#    define _POSIX_C_SOURCE      200112
#  elif _XOPEN_SOURCE - 0 >= 500
#    define __XSI_VISIBLE           500
#    undef _POSIX_C_SOURCE
#    define _POSIX_C_SOURCE      199506
#  endif
#endif

/*
 * Deal with all versions of POSIX. The ordering relative to the tests above is
 * important.
 */
#if defined(_POSIX_SOURCE) && !defined(_POSIX_C_SOURCE)
#  define _POSIX_C_SOURCE        198808
#endif

#ifdef _POSIX_C_SOURCE
#  if _POSIX_C_SOURCE >= 200809
#    define __POSIX_VISIBLE        200809
#    define __ISO_C_VISIBLE          1999
#  elif _POSIX_C_SOURCE >= 200112
#    define __POSIX_VISIBLE        200112
#    define __ISO_C_VISIBLE          1999
#  elif _POSIX_C_SOURCE >= 199506
#    define __POSIX_VISIBLE        199506
#    define __ISO_C_VISIBLE          1990
#  elif _POSIX_C_SOURCE >= 199309
#    define __POSIX_VISIBLE        199309
#    define __ISO_C_VISIBLE          1990
#  elif _POSIX_C_SOURCE >= 199209
#    define __POSIX_VISIBLE        199209
#    define __ISO_C_VISIBLE          1990
#  elif _POSIX_C_SOURCE >= 199009
#    define __POSIX_VISIBLE        199009
#    define __ISO_C_VISIBLE          1990
#  else
#    define __POSIX_VISIBLE        198808
#    define __ISO_C_VISIBLE             0
#  endif
#else
/*
 * Deal with _ANSI_SOURCE:
 * If it is defined, and no other compilation environment is explicitly requested,
 * then define our internal feature-test macros to zero. This makes no difference
 * to the processor (undefined symbols in preprocessing expressions are defined to
 * have value zero), but makes it more convenient for a test program to print out
 * the values.
 *
 * If a program mistakenly defines _ANSI_SOURCE and some other macro such as
 * _POSIX_C_SOURCE, we'll assume that it wants the broader compilation environment
 * (and in fact we'll never get here).
 */
#  if defined(_ANSI_SOURCE)          /* Hide almost everything */
#    define __POSIX_VISIBLE          0
#    define __XSI_VISIBLE            0
#    define __BSD_VISIBLE            0
#    define __ISO_C_VISIBLE       1990
#    define __EXT1_VISIBLE           0
#  elif defined(_C99_SOURCE)         /* Localism to specify strict C99 environment */
#    define __POSIX_VISIBLE          0
#    define __XSI_VISIBLE            0
#    define __BSD_VISIBLE            0
#    define __ISO_C_VISIBLE       1999
#    define __EXT1_VISIBLE           0
#  elif defined(_C11_SOURCE)         /* Localism to specify strict C11 environment */
#    define __POSIX_VISIBLE          0
#    define __XSI_VISIBLE            0
#    define __BSD_VISIBLE            0
#    define __ISO_C_VISIBLE       2011
#    define __EXT1_VISIBLE           0
#  else                              /* Default environment: show everything */
#    define __POSIX_VISIBLE     200809
#    define __XSI_VISIBLE          700
#    define __BSD_VISIBLE            1
#    define __ISO_C_VISIBLE       2011
#    define __EXT1_VISIBLE           1
#  endif
#endif

/*
 * User override __EXT1_VISIBLE
 */
#if defined(__STDC_WANT_LIB_EXT1__)
#  undef __EXT1_VISIBLE
#  if __STDC_WANT_LIB_EXT1__
#    define __EXT1_VISIBLE           1
#  else
#    define __EXT1_VISIBLE           0
#  endif
#endif



#endif
