/**
 * @file INU/readtable/hash/hashtable.c
 */
#include "hashtable.h"

/*------------------------------------------------------
 * cds_list inline prototypes
 *------------------------------------------------------*/
void cds_list_add(struct cds_list_head [static 1], struct cds_list_head [static 1]);
void cds_list_add_tail(struct cds_list_head [static 1], struct cds_list_head [static 1]);
void __cds_list_del(struct cds_list_head [static 1], struct cds_list_head [static 1]);
void cds_list_del(struct cds_list_head [static 1]);
void cds_list_del_init(struct cds_list_head [static 1]);
void cds_list_move(struct cds_list_head [static 1], struct cds_list_head [static 1]);
void cds_list_replace(struct cds_list_head [static 1], struct cds_list_head [static 1]);
void cds_list_splice(struct cds_list_head [static 1], struct cds_list_head [static 1]);
int  cds_list_empty(struct cds_list_head [static 1]);
void cds_list_replace_init(struct cds_list_head [static 1], struct cds_list_head [static 1]);

/**
 * @brief Map from hash value to corresponding bucket.
 */
#define HASH2BKT(HTP, H) (&(HTP)->ht_bucket[H % (HTP)->ht_nbuckets])

inu_hashtable *inu_hashtable_new(size_t nbuckets, int (*cmp)(struct htelem htep[static 1], void *key))
{
    inu_hashtable *ht = malloc(sizeof *ht + nbuckets * sizeof(struct htbucket));
    if (!ht) return (void *) 0;

    ht->ht_nbuckets = nbuckets;
    ht->ht_cmp      = cmp;

    for (size_t i = 0; i < nbuckets; i++) {
        CDS_INIT_LIST_HEAD(&ht->ht_bucket[i].htbucket_head);
    }

    return ht;
}

void inu_hashtable_destroy(inu_hashtable ht[static 1])
{
    free(ht);
}

void inu_hashtable_add(inu_hashtable ht[static 1], size_t hash, struct htelem htep[static 1])
{
    htep->htelem_hash = hash;
    cds_list_add(&htep->htelem_next, &HASH2BKT(ht, hash)->htbucket_head);
}

void inu_hashtable_del(struct htelem htep[static 1])
{
    cds_list_del_init(&htep->htelem_next);
}

struct htelem *inu_hashtable_lookup(inu_hashtable ht[static 1], size_t hash, void *key)
{
    struct htelem *htep;

    cds_list_for_each_entry(htep, &HASH2BKT(ht, hash)->htbucket_head, htelem_next) {
        if (htep->htelem_hash != hash) continue;
        if (ht->ht_cmp(htep, key)) return htep;
    }
    return (void *) 0;
}

void inu_hashtable_lookup_done(struct htelem htep[static 1])
{
    /* Empty for now */
}
