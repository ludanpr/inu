/**
 * @brief INU/readtable/hash/hashtable.h
 */
#ifndef INU_HASH_TABLE_H_
#define INU_HASH_TABLE_H_ 1

#include "cds_list.h"
#include <stdlib.h>

/**
 * Hash-table element to be included in structures in a hash table.
 */
struct htelem {
    struct cds_list_head htelem_next;
    size_t htelem_hash;
};

/**
 * Hash-table bucket element.
 */
struct htbucket {
    struct cds_list_head htbucket_head;
};

/**
 * @brief Top-level hash-table data structure.
 */
typedef struct {
    size_t ht_nbuckets;
    int (*ht_cmp)(struct htelem htep[static 1], void *key);
    struct htbucket ht_bucket[FLEX_ARRAY];
} inu_hashtable;


/**
 * @brief Allocate a new hash-table with the @c nbuckets buckets.
 */
inu_hashtable *inu_hashtable_new(size_t nbuckets, int (*cmp)(struct htelem htep[static 1], void *key));

/**
 * @brief Deallocate a hash-table.
 */
void inu_hashtable_destroy(inu_hashtable ht[static 1]);

/**
 * @brief Add an element to the hash-table.
 *
 * @remark Caller must have acquired the update-side lock
 * via inu_hashtable_lock_mod().
 */
void inu_hashtable_add(inu_hashtable ht[static 1], size_t hash, struct htelem htep[static 1]);

/**
 * @brief Remove the specified element from the hash-table. The
 * pointer to the actual element must be already in place in @c htep.
 *
 * @remark Caller must have acquired the update-side lock via
 * inu_hashtable_lock_mod().
 */
void inu_hashtable_del(struct htelem htep[static 1]);

/**
 * @brief Look up a key.
 *
 * @remark Caller must have acquired either a read-side or update-side
 * lock via either inu_hashtable_lock_lookup() or inu_hashtable_lock_mode().
 *
 * @remark Note that the return is a pointer to the @c htelem: Use offsetof()
 * or equivalent to get a pointer to the full data structure.
 *
 * @remark Note that the caller is responsible for mapping from whatever
 * type of key is in use to an @c size_t, passed via @c hash.
 */
struct htelem *inu_hashtable_lookup(inu_hashtable ht[static 1], size_t hash, void *key);

/**
 * @brief Finished using a looked up hash-table element.
 */
void inu_hashtable_lookup_done(struct htelem htep[static 1]);


#endif
