/**
 * @file INU/readtable/hash/cds_list.h
 *
 * Copyright (C) 2002 Free Software Foundation, Inc.
 * (originally part of the GNU C Library)
 * Contributed by Ulrich Drepper <drepper@redhat.com>, 2002.
 *
 * Copyright (C) 2009 Pierre-Marc Fournier
 * Conversion to RCU list.
 * Copyright (C) 2010 Mathieu Desnoyers <mathieu.desnoyers@efficios.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */
#ifndef CDS_LIST_H_
#define CDS_LIST_H_ 1

#include <common.h>

/**
 * @brief Circular double-linked list.
 *
 * +---+---+---+---+---+---+
 * |   |   |   |   |   |   |
 * +---+---+---+---+---+---+
 * tail                 head
 */
struct cds_list_head {
    struct cds_list_head *next;   /* -->  */
    struct cds_list_head *prev;   /* <--  */
};

/**
 * @brief Defines a variable with the head and tail of the list.
 */
#define CDS_LIST_HEAD(NAME)                                   \
    struct cds_list_head NAME = { &(NAME), &(NAME), }

/**
 * @brief Initializes a new list head.
 */
#define CDS_INIT_LIST_HEAD(PTR)                               \
    (PTR)->next = (PTR)->prev = (PTR)

#define CDS_LIST_HEAD_INIT(NAME) { .prev = &(NAME), .next = &(NAME), }

/**
 * @brief Add a new element at the head of the list
 */
static inline void cds_list_add(struct cds_list_head newp[static 1], struct cds_list_head head[static 1])
{
    head->next->prev = newp;
    newp->next       = head->next;
    newp->prev       = head;
    head->next       = newp;
}

/**
 * @brief Add a new element at the tail of the list.
 */
static inline void cds_list_add_tail(struct cds_list_head newp[static 1], struct cds_list_head tail[static 1])
{
    tail->prev->next = newp;
    newp->next       = tail;
    newp->prev       = tail->prev;
    tail->prev       = newp;
}

/**
 * @brief Remove element from list.
 */
static inline void __cds_list_del(struct cds_list_head prev[static 1], struct cds_list_head next[static 1])
{
    next->prev = prev;
    prev->next = next;
}

static inline void cds_list_del(struct cds_list_head elem[static 1])
{
    __cds_list_del(elem->prev, elem->next);
}

/**
 * @brief Remove element from list, initializing the element's list pointers.
 */
static inline void cds_list_del_init(struct cds_list_head elem[static 1])
{
    cds_list_del(elem);
    CDS_INIT_LIST_HEAD(elem);
}

/**
 * @brief Delete from list, add to another list as its head.
 */
static inline void cds_list_move(struct cds_list_head elem[static 1], struct cds_list_head head[static 1])
{
    __cds_list_del(elem->prev, elem->next);
    cds_list_add(elem, head);
}

/**
 * @brief Replace an old entry.
 */
static inline void cds_list_replace(struct cds_list_head oldp[static 1], struct cds_list_head newp[static 1])
{
    newp->prev       = oldp->prev;
    newp->next       = oldp->next;
    newp->prev->next = newp;
    newp->next->prev = newp;
}

/**
 * @brief Joins two lists.
 */
static inline void cds_list_splice(struct cds_list_head add[static 1], struct cds_list_head head[static 1])
{
    if (add != add->next) {  /* do nothing if add is empty */
        add->next->prev  = head;
        add->prev->next  = head->next;
        head->next->prev = add->prev;
        head->next       = add->next;
    }
}

#define cds_list_entry(PTR, TYPE, MEMBER) container_of(PTR, TYPE, MEMBER)
/**
 * @brief Get first entry from a list.
 */
#define cds_list_first_entry(PTR, TYPE, MEMBER) cds_list_entry((PTR)->next, TYPE, MEMBER)

/**
 * @brief Iterate forward over the elements of the list.
 */
#define cds_list_for_each(POS, HEAD) for (POS = (HEAD)->next; (POS) != (HEAD); POS = (POS)->next)

/**
 * @brief Iterate forward over the elements of the list. The list elements
 * can be removed from the list while doing this.
 */
#define cds_list_for_each_safe(POS, P, HEAD) for (POS = (HEAD)->next, P = (POS)->next; (POS) != (HEAD); POS = (P), P = (POS)->next)

/**
 * @brief Iterate backwards over the elements of the list.
 */
#define cds_list_for_each_prev(POS, HEAD) for (POS = (HEAD)->prev; (POS) != (HEAD); POS = (POS)->prev)

/**
 * @brief Iterate backwards over the elements of the list. The list elements
 * can be removed from the list while doing this.
 */
#define cds_list_for_each_prev_safe(POS, P, HEAD) for (POS = (HEAD)->prev, P = (POS)->prev; (POS) != (HEAD); POS = (P), P = (POS)->prev)

/**
 * @warning Uses non-Standard `__typeof__`
 */
#define cds_list_for_each_entry(POS, HEAD, MEMBER)                                    \
    for (POS = cds_list_entry((HEAD)->next, __typeof__(*(POS)), MEMBER);              \
         &(POS)->MEMBER != (HEAD);                                                    \
         POS = cds_list_entry((POS)->MEMBER.next, __typeof__(*(POS)), MEMBER))

#define cds_list_for_each_entry_reverse(POS, HEAD, MEMBER)                            \
    for (POS = cds_list_entry((HEAD)->prev, __typeof__(*(POS)), MEMBER);              \
         &(POS)->MEMBER != (HEAD);                                                    \
         POS = cds_list_entry((POS)->MEMBER.prev, __typeof__(*(POS)), MEMBER))

#define cds_list_for_each_entry_safe(POS, P, HEAD, MEMBER)                            \
    for (POS = cds_list_entry((HEAD)->next, __typeof__(*(POS)), MEMBER),              \
             P = cds_list_entry((POS)->member.next, __typeof__(*(POS)), MEMBER);      \
         &(POS)->MEMBER != (HEAD);                                                    \
         POS = (P), P = cds_list_entry((POS)->MEMBER.next, __typeof__(*(POS)), MEMBER))

/**
 * @brief Same as cds_list_for_each_entry_safe, but starts from "POS", which
 * should point to an entry within the list.
 */
#define cds_list_for_each_entry_safe_from(POS, P, HEAD, MEMBER)                       \
    for (P = cds_list_entry((POS)->MEMBER.next, __typeof__(*(POS)), MEMBER);          \
         &(POS)->MEMBER != (HEAD);                                                    \
         POS = (P), P = cds_list_entry((POS)->MEMBER.next, __typeof__(*(POS)), MEMBER))

static inline int cds_list_empty(struct cds_list_head head[static 1])
{
    return head == head->next;
}

/**
 * @brief Replace @c oldp for @c newp and initializes a list
 * in @c oldp.
 */
static inline void cds_list_replace_init(struct cds_list_head oldp[static 1], struct cds_list_head newp[static 1])
{
    struct cds_list_head *head = oldp->next;

    cds_list_del(oldp);
    cds_list_add_tail(newp, head);
    CDS_INIT_LIST_HEAD(oldp);
}

#endif
